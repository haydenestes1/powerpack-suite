#!/bin/bash

SAMPLES_PER_SEC=1000
SAMPLING_TIME_IN_SECS=40
START_BENCH_DELAY_SECS=2
OUTPUT_FILE=MLPerf_Inference_DAQmx_PerformanceV3.csv

echo "Starting MLPerf Inference Performance! Writing to file: ${OUTPUT_FILE}"

#Take NIDAQmx Measurements (in the background)
/home/cesar/Desktop/testNIDAQ_CesarV2/DiskDriveTest/main $SAMPLES_PER_SEC $SAMPLING_TIME_IN_SECS $OUTPUT_FILE > ${OUTPUT_FILE} &
waitpid=$!

# Start MLPerf Inference Benchmark
cm run script "app mlperf inference generic _python _retinanet _onnxruntime _cpu" \
     --scenario=Offline \
     --mode=performance \
     --rerun \
     --num_threads=4

# Wait some seconds and then take NIDAQmx Measurements (this avoids checking dependencies)
#sleep $START_BENCH_DELAY_SECS

#Take NIDAQmx Measurements
#./testNIDAQ_CesarV2/main $SAMPLES_PER_SEC $SAMPLING_TIME_IN_SECS $OUTPUT_FILE > ${OUTPUT_FILE}

echo "MLPerf Inference DAQmx Performance Complete!"
echo "Waiting for sampling to finish..."

# Wait for sampling to finish
wait $waitpid

#echo "Generating image..."

#python3 ./plotdata.py

#xdg-open allLines.jpg
#xdg-open meanLines.jpg

echo "MLPerf Inference DAQmx Performance complete!"
