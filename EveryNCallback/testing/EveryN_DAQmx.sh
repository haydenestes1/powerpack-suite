#!/bin/bash

SAMPLES_PER_SEC=1000
SAMPLING_TIME_IN_SECS=15
START_BENCH_DELAY_SECS=2
#OUTPUT_FILE=CPU_5Lines_SysbenchV12_Output.csv11
#OUTPUT_FILE=CPU_7Lines_NewDAQ_AllCalibratedV4_Output.csv
OUTPUT_FILE=OutputV1.csv


echo "Starting EveryNCallback testing! Writing to file: ${OUTPUT_FILE}"

#Take NIDAQmx Measurements (in the background)
/home/cesar/Desktop/testNIDAQ_CesarV2/EveryNCallback/main $SAMPLES_PER_SEC $SAMPLING_TIME_IN_SECS $OUTPUT_FILE > ${OUTPUT_FILE} &
#/home/cesar/Desktop/testNIDAQ_CesarV2/main $SAMPLES_PER_SEC $SAMPLING_TIME_IN_SECS $OUTPUT_FILE > ${OUTPUT_FILE} &
waitpid=$!

# Start Benchmark
#sysbench --test=cpu run

echo "CPU Only Sysbench Complete!"
echo "Waiting for sampling to finish..."

wait $waitpid

echo "EveryNCallback test Complete!"
