
#include <stdlib.h>
#include <stdio.h>
#include <NIDAQmx.h>

//Includes for writing to temp files
#include<unistd.h>
#include<string.h>
#include<errno.h>

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// This is the name of the DAQ device
//#define DAQ_DEVICE_NAME "cDAQ1"
#define DAQ_DEVICE_NAME "cDAQ2"

// This is the name of the module port we measure from
#define DAQ_MODULE_NAME "Mod3"  //12V
#define DAQ_MODULE_NAME2 "Mod6"  //5V
#define DAQ_MODULE_NAME3 "Mod8"  //3V

//#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME
#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME

// The END channel is INCLUSIVE
// These are the pin indices used for DIFFERENTIAL mode
// Pin X will automatically be paired with its corresponding
// x+8 pin. 
// This setup assumes we are using pins 0, 1, 2, and 3
// with their complementary pins 8, 9, 10, and 11
// which are automatically read by the NIDAQ library
// when we are in differential mode
#define DIFF_INPUTS_BEGIN_V1 "0"
//#define DIFF_INPUTS_END   "3"
#define DIFF_INPUTS_END_V1   "4"

#define DIFF_INPUTS_BEGIN_V2 "0"
#define DIFF_INPUTS_END_V2   "0"

#define DIFF_INPUTS_BEGIN_V3 "0"
#define DIFF_INPUTS_END_V3   "0"

// We'll pass this into the CreateAIVoltageChan to specify
// the channels we want to work with. 
//#define DIFF_CHANNELS DAQ_DEVICE "/ai" DIFF_INPUTS_BEGIN_V1 ":" DIFF_INPUTS_END_V1 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME2 "/ai" DIFF_INPUTS_BEGIN_V2 ":" DIFF_INPUTS_END_V2
#define DIFF_CHANNELS DAQ_DEVICE "/ai" DIFF_INPUTS_BEGIN_V1 ":" DIFF_INPUTS_END_V1 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME2 "/ai" DIFF_INPUTS_BEGIN_V2 ":" DIFF_INPUTS_END_V2 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME3 "/ai" DIFF_INPUTS_BEGIN_V3 ":" DIFF_INPUTS_END_V3
#define PHYS_CHANNELS DIFF_CHANNELS

// The name assigned to our task
#define DAQ_TASK_NAME ""

// The name we assign to our voltage channel
#define CHANNEL_NAME ""

// The minimum and maximum volts we expect to measure
// The NIDAQ device only supports max of -10/10 volts
// This is not good if we have a 12V supply we want to measure
#define MIN_VOLTS -10.0
#define MAX_VOLTS  10.0

// Number of samples to collect each second for each channel
//#define SAMPLES_PER_SEC 1000

// The number of samples we want to take for each channel
//#define SAMPLES_PER_CHANNEL 16000
//#define SAMPLES_PER_CHANNEL 30000

// The amount of time to wait to read the samples
#define SAMPLES_WAIT_TIMEOUT_SECS 100

// The number of differential channel pairs we will read from
//#define NUM_CHANNEL_PAIRS 4
#define NUM_CHANNEL_PAIRS 7

// The number of samples we expect to collect
#define ARRAY_SIZE_IN_SAMPLES NUM_CHANNEL_PAIRS*SAMPLES_PER_CHANNEL

// The resistance of the resister that we measure the voltage diff over
#define RESISTOR_OHMS 0.003

// The voltage we assume that all the lines are running at
#define LINE_VOLTAGE 12

//#define ARRAY_SIZE 7
//#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}
#define ARRAY_SIZE 7
#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}

FILE *everyN_fp;

// Function to convert voltage differential to power
void nidaqDiffVoltToPower(float64 *result, float64 *readings);

// Function prototype for EveryNCallback
int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);


int DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData) {
  int32 error = 0;
  char errBuff[2048] = {'\0'};

  // Check to see if an error stopped the task.
  DAQmxErrChk(status);

Error:
  if (DAQmxFailed(error)) {
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    DAQmxClearTask(taskHandle);
    printf("DAQmx Error: %s\n", errBuff);
  }
  if( taskHandle != 0 )  {
    printf("DAQ task finished without error\n");
    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);
  }
  return 0;
}

int main(int argc, char** argv){

  //Should contain 
  // - DAQmxCreateTask
  // - DAQmxCreateAIVoltageChan
  // - DAQmxCfgSampClkTiming
  // - DAQmxRegisterEveryNSamplesEvent
  // - DAQmxRegisterDoneEvent
  // - DAQmxStartTask

  float voltage_array[ARRAY_SIZE] = INIT_ARRAY;
  
  if(argc != 4){
    printf("Invalid input argument count. Given %d, Expected %d\n", argc-1, 2);
    printf("Usage: ./main [SAMPLES_PER_SECOND] [SAMPLING_TIME_IN_SECONDS]\n");
    exit(-1);
  }

  int SAMPLES_PER_SEC = atoi(argv[1]);
  int SAMPLING_SECS = atoi(argv[2]);
  int SAMPLES_PER_CHANNEL = SAMPLING_SECS*SAMPLES_PER_SEC;

  /*
  FILE *fp;
  fp = fopen(argv[3], "w");
  FILE *data_fp;
  data_fp = fopen("CPU_7Lines_Data_OutputV1.csv", "w");
  */

  int32       error = 0;
  TaskHandle  taskHandle = 0;
  int32       samples_read_per_channel;
  float64     data[ARRAY_SIZE_IN_SAMPLES];
  char        errBuff[2048]={'\0'};

  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/
  DAQmxErrChk (DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle));

  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, 
                                       PHYS_CHANNELS, 
                                       CHANNEL_NAME, 
                                       DAQmx_Val_Cfg_Default,
                                       MIN_VOLTS, MAX_VOLTS, 
                                       DAQmx_Val_Volts, NULL));

  DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, NULL, SAMPLES_PER_SEC, 
                                    DAQmx_Val_Rising, 
                                    DAQmx_Val_ContSamps,
                                    //DAQmx_Val_FiniteSamps, 
                                    SAMPLES_PER_CHANNEL ));

  DAQmxErrChk(DAQmxRegisterEveryNSamplesEvent(taskHandle, DAQmx_Val_Acquired_Into_Buffer, 
                                  1000, 0, EveryNCallback, NULL));  

  DAQmxErrChk(DAQmxRegisterDoneEvent(taskHandle, 0, DoneCallback, NULL));

  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  DAQmxErrChk(DAQmxStartTask(taskHandle));

  Error:
  if (DAQmxFailed(error)) {
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    printf("DAQmx Error: %s\n", errBuff);
  }
  return 0;
}

/**
 * Converts voltage differential to power
 *
 * @param result location for storage of results
 * @param readings voltage differential measurements from the ni meter
 */
void nidaqDiffVoltToPower(float64 *result, float64 *readings) {

  float64 voltages[] = INIT_ARRAY;
  int i;
  for (i = 0; i < NUM_CHANNEL_PAIRS; i++) {
      result[i] = (readings[i] / RESISTOR_OHMS) * (voltages[i] - readings[i]);
  }
}

int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData) {
  //FILE *everyN_fp;
  //everyN_fp = fopen("EveryN_OutputV1.txt", "w");
  printf("---> EveryNCallback() Called <---\n");

  int32 error = 0;
  char errBuff[2048] = {'\0'};
  int32 samplesRead = 0;
  int data_array_size = 60000; //Needs to be manually updated 
  //int data_array_size = ARRAY_SIZE_IN_SAMPLES;
  float64 *data = (float64*)malloc(data_array_size * sizeof(float64));
  //float64 data[data_array_size];
  float64 channels[NUM_CHANNEL_PAIRS];
  float64 powerReadings[NUM_CHANNEL_PAIRS];
  //char dataString[bufferSize];  //Remove and figure out different approach

  //Should contain 
  // - DAQmxReadAnalogF64
  // - Reading the samples & taking averages 
  // - Calculating voltage differences 

  /*********************************************/
  // DAQmx Read Code
  /*********************************************/
  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 0, DAQmx_Val_GroupByChannel,
                                 data, data_array_size, &samplesRead, NULL));


  if (samplesRead > 0) {
    int i, j;  // Move variable declarations to the beginning of the block
    // Get the average reading for each channel
    for (i = 0; i < NUM_CHANNEL_PAIRS; i++) {
      channels[i] = 0.0;
      for (j = 0; j < samplesRead; j++) {
        channels[i] += data[j + i * samplesRead];
      }
      channels[i] /= samplesRead;
    }
  }

  if (samplesRead > 0) {
    printf("Acquired %d samples.\n", samplesRead);
  }

  nidaqDiffVoltToPower(powerReadings, channels);

  int k;
  for(k = 0; k < NUM_CHANNEL_PAIRS; k++) {
    printf("%2.6f, ", powerReadings[k]);
  }
  printf("\n");
  //free(data);

  Error:
  if (DAQmxFailed(error)) {
    // Get and print error information
    DAQmxGetExtendedErrorInfo(errBuff, 2048);

    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);
    printf("DAQmx Error: %s\n", errBuff);
  }
  return 0;
}
