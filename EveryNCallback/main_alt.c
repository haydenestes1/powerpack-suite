#include <stdio.h>
#include <stdlib.h>
#include <NIDAQmx.h>

#define NIDAQ_CHAN_RESISTOR 0.003

typedef struct {
    int numChannels;
    int sampleRate;
    int bufferSize;
    char* channelDescription;
    double* channelVoltages;
} Configuration;

typedef struct {
    FILE* writer;
    char* logFile;
    Configuration config;
    TaskHandle taskHandle;
    int totalSamplesRead;
} NIDAQmxEventHandler;

void nidaqDiffVoltToPower(double* result, double* readings, double* voltages, size_t numChannels) {
    for (size_t i = 0; i < numChannels; i++) {
        result[i] = (readings[i] / NIDAQ_CHAN_RESISTOR) * (voltages[i] - readings[i]);
    }
}

int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void* callbackData) {
    NIDAQmxEventHandler* handler = (NIDAQmxEventHandler*)callbackData;
    int numChannels = handler->config.numChannels;
    int bufferSize = handler->config.bufferSize;

    int32 error = 0;
    char errBuff[2048] = {'\0'};
    int32 samplesRead = 0;
    double data[bufferSize];
    char* dataString = NULL;
    double channels[numChannels];
    double powerReadings[numChannels];

    DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 0, DAQmx_Val_GroupByChannel, data, bufferSize, &samplesRead, NULL));

    if (samplesRead > 0) {
        // Get the average reading for each channel
        for (int i = 0; i < numChannels; i++) {
            channels[i] = 0.0;
            for (int j = 0; j < samplesRead; j++) {
                channels[i] += data[j + i * samplesRead];
            }
            channels[i] /= samplesRead;
        }
    }

    if (samplesRead > 0) {
        printf("Acquired %d samples. Total %d\r", samplesRead, handler->totalSamplesRead += samplesRead);
        fflush(stdout);
    }

    nidaqDiffVoltToPower(powerReadings, channels, handler->config.channelVoltages, numChannels);

    asprintf(&dataString, "%f %f %f\n", powerReadings[0], powerReadings[1], powerReadings[2]);

    fprintf(handler->writer, "%s", dataString);
    fflush(handler->writer);
    free(dataString);

Error:
    if (DAQmxFailed(error)) {
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
        DAQmxGetExtendedErrorInfo(errBuff, 2048);
        printf("DAQmx Error: %s\n", errBuff);
    }
    return 0;
}

int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void* callbackData) {
    int32 error = 0;
    char errBuff[2048] = {'\0'};

    DAQmxErrChk(status);

Error:
    if (DAQmxFailed(error)) {
        DAQmxClearTask(taskHandle);
        DAQmxGetExtendedErrorInfo(errBuff, 2048);
        printf("DAQmx Error: %s\n", errBuff);
    }
    return 0;
}

void configure(NIDAQmxEventHandler* handler, Configuration configuration) {
    handler->config.numChannels = atoi(configuration.channelDescription);
    handler->config.sampleRate = atoi(configuration.sampleRate);
    handler->config.bufferSize = handler->config.numChannels * handler->config.sampleRate;
    handler->config.channelDescription = configuration.channelDescription;
    handler->config.channelVoltages = NULL; // You need to parse the channel voltages from the configuration
}

void startHandler(NIDAQmxEventHandler* handler, uint64_t timestamp) {
    // Implementation of startHandler
    // ...

    DAQmxCreateTask("", &(handler->taskHandle));
    // Add the DAQmx Configure Code
    // ...

    DAQmxStartTask(handler->taskHandle);
}

void endHandler(NIDAQmxEventHandler* handler, uint64_t timestamp) {
    // Implementation of endHandler
    // ...

    DAQmxStopTask(handler->taskHandle);
    DAQmxClearTask(handler->taskHandle);

    // Print timestamps
    // ...
}

int main() {
    NIDAQmxEventHandler handler;
    Configuration configuration; // You need to set up the configuration

    handler.writer = fopen("logfile.txt", "w");
    handler.logFile = "logfile.txt";
    configure(&handler, configuration);

    uint64_t timestamp = 0; // You need to set up the timestamp

    startHandler(&handler, timestamp);

    // Simulate some operations
    for (int i = 0; i < 10; i++) {
        Sleep(1000); // Sleep for 1 second (you need to include windows.h for Sleep)
    }

    endHandler(&handler, timestamp);

    fclose(handler.writer);
    return 0;
}
