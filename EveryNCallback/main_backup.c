
#include <stdlib.h>
#include <stdio.h>
#include <NIDAQmx.h>

//Includes for writing to temp files
#include<unistd.h>
#include<string.h>
#include<errno.h>

//#include <cstdint>

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// This is the name of the DAQ device
//#define DAQ_DEVICE_NAME "cDAQ1"
#define DAQ_DEVICE_NAME "cDAQ2"

// This is the name of the module port we measure from
#define DAQ_MODULE_NAME "Mod3"  //12V
#define DAQ_MODULE_NAME2 "Mod6"  //5V
#define DAQ_MODULE_NAME3 "Mod8"  //3V

//#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME
#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME

// The END channel is INCLUSIVE
// These are the pin indices used for DIFFERENTIAL mode
// Pin X will automatically be paired with its corresponding
// x+8 pin. 
// This setup assumes we are using pins 0, 1, 2, and 3
// with their complementary pins 8, 9, 10, and 11
// which are automatically read by the NIDAQ library
// when we are in differential mode
#define DIFF_INPUTS_BEGIN_V1 "0"
//#define DIFF_INPUTS_END   "3"
#define DIFF_INPUTS_END_V1   "4"

#define DIFF_INPUTS_BEGIN_V2 "0"
#define DIFF_INPUTS_END_V2   "0"

#define DIFF_INPUTS_BEGIN_V3 "0"
#define DIFF_INPUTS_END_V3   "0"

// We'll pass this into the CreateAIVoltageChan to specify
// the channels we want to work with. 
//#define DIFF_CHANNELS DAQ_DEVICE "/ai" DIFF_INPUTS_BEGIN_V1 ":" DIFF_INPUTS_END_V1 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME2 "/ai" DIFF_INPUTS_BEGIN_V2 ":" DIFF_INPUTS_END_V2
#define DIFF_CHANNELS DAQ_DEVICE "/ai" DIFF_INPUTS_BEGIN_V1 ":" DIFF_INPUTS_END_V1 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME2 "/ai" DIFF_INPUTS_BEGIN_V2 ":" DIFF_INPUTS_END_V2 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME3 "/ai" DIFF_INPUTS_BEGIN_V3 ":" DIFF_INPUTS_END_V3
#define PHYS_CHANNELS DIFF_CHANNELS

// The name assigned to our task
#define DAQ_TASK_NAME ""

// The name we assign to our voltage channel
#define CHANNEL_NAME ""

// The minimum and maximum volts we expect to measure
// The NIDAQ device only supports max of -10/10 volts
// This is not good if we have a 12V supply we want to measure
#define MIN_VOLTS -10.0
#define MAX_VOLTS  10.0

// Number of samples to collect each second for each channel
//#define SAMPLES_PER_SEC 1000

// The number of samples we want to take for each channel
//#define SAMPLES_PER_CHANNEL 16000
//#define SAMPLES_PER_CHANNEL 30000

// The amount of time to wait to read the samples
#define SAMPLES_WAIT_TIMEOUT_SECS 100

// The number of differential channel pairs we will read from
//#define NUM_CHANNEL_PAIRS 4
#define NUM_CHANNEL_PAIRS 7

// The number of samples we expect to collect
#define ARRAY_SIZE_IN_SAMPLES NUM_CHANNEL_PAIRS*SAMPLES_PER_CHANNEL

// The resistance of the resister that we measure the voltage diff over
#define RESISTOR_OHMS 0.003

// The voltage we assume that all the lines are running at
#define LINE_VOLTAGE 12

//#define ARRAY_SIZE 7
//#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}
#define ARRAY_SIZE 7
#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}

#define SAMPLES_PER_SEC atoi(argv[1])
#define SAMPLING_SECS atoi(argv[2])
#define SAMPLES_PER_CHANNEL SAMPLING_SECS*SAMPLES_PER_SEC
#define BUFFER_SIZE NUM_CHANNEL_PAIRS*SAMPLES_PER_SEC


//int32_t EveryNCallback(TaskHandle taskHandle, int32_t everyNsamplesEventType, uint32_t nSamples, void *callbackData) {
int EveryNCallback(TaskHandle taskHandle, int everyNsamplesEventType, int nSamples, int bufferSize) {


    printf("---> EveryNCallback() Called <---\n");
    
    int32 error = 0;
    //int SAMPLES_PER_SEC = atoi(argv[1]);
    //int SAMPLING_SECS = atoi(argv[2]);
    //int SAMPLES_PER_CHANNEL = SAMPLING_SECS*SAMPLES_PER_SEC;
    //int32 bufferSize = SAMPLES_PER_CHANNEL;
    char errBuff[2048] = {'\0'};
    int32 samplesRead = 0;
    float64 data[bufferSize];
    float64 channels[NUM_CHANNEL_PAIRS];

    /*********************************************/
    // DAQmx Read Code
    /*********************************************/
    /*
    DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, -1, 0, DAQmx_Val_GroupByChannel,
                                 data, bufferSize, &samplesRead, NULL));
    
    if (samplesRead > 0) {
      // Get the average reading for each channel
      for (int i = 0; i < NUM_CHANNEL_PAIRS; i++) {
        channels[i] = 0.0;
        for (int j = 0; j < samplesRead; j++) {
        channels[i] += data[j + i * samplesRead];
        }
        channels[i] /= samplesRead;
      }
    }

    if (samplesRead > 0) {
    printf("Acquired %d samples\n", samplesRead);
    //fflush(stdout);
    }


    */
   /*

  Error:
  if (DAQmxFailed(error)) {
    // Get and print error information
    DAQmxGetExtendedErrorInfo(errBuff, 2048);

    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);
    printf("DAQmx Error: %s\n", errBuff);
  }
  */
  return 0;

}

//int32_t DoneCallback(TaskHandle taskHandle, int32_t status, void *callbackData) {
int DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData) {
  int32 error = 0;
  char errBuff[2048] = {'\0'};

  // Check to see if an error stopped the task.
  DAQmxErrChk(status);

Error:
  if (DAQmxFailed(error)) {
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    DAQmxClearTask(taskHandle);
    printf("DAQmx Error: %s\n", errBuff);
  }
  return 0;
}

int main(int argc, char** argv) {
  printf("--> main.c's main() called <--\n");

  float voltage_array[ARRAY_SIZE] = INIT_ARRAY;

  //printf("Starting tests of NIDAQ unit...\n");
  //printf("Using device: [%s]\n", DAQ_DEVICE);
  //printf("Using channels: [%s]\n", PHYS_CHANNELS);
  
  if(argc != 4){
    printf("Invalid input argument count. Given %d, Expected %d\n", argc-1, 2);
    printf("Usage: ./main [SAMPLES_PER_SECOND] [SAMPLING_TIME_IN_SECONDS]\n");
    exit(-1);
  }

  //int SAMPLES_PER_SEC = atoi(argv[1]);
  //int SAMPLING_SECS = atoi(argv[2]);
  //int SAMPLES_PER_CHANNEL = SAMPLING_SECS*SAMPLES_PER_SEC;

  //-- open input file --
  FILE *fp;
  fp = fopen(argv[3], "w");

  int32       error = 0;
  TaskHandle  taskHandle = 0;
  int32       samples_read_per_channel;
  float64     data[ARRAY_SIZE_IN_SAMPLES];
  char        errBuff[2048]={'\0'};

  //---- Printing output to CSV in temp -----
  //FILE *fpt;
  //fpt = fopen("Main_Output.csv", "w+");



 //---- Printing output to CSV in temp -----

  //printf("Starting task creation!\n");

  DAQmxErrChk (DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle));

  //printf("Task created!\n");

  // Start in differential mode
  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, 
                                       PHYS_CHANNELS, 
                                       //"cDAQ1Mod3/ai0:3, cDAQ1Mod3/ai8:11",
                                       CHANNEL_NAME, 
                                       //DAQmx_Val_Diff, 
                                       DAQmx_Val_Cfg_Default,
                                       //DAQmx_Val_NRSE,
                                       //DAQmx_Val_RSE,
                                       MIN_VOLTS, MAX_VOLTS, 
                                       DAQmx_Val_Volts, NULL));

  //printf("Voltage Chan created!\n");

  // Setup the sample clock and the rate at which we collect samples
  DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, NULL, SAMPLES_PER_SEC, 
                                    DAQmx_Val_Rising, 
                                    DAQmx_Val_FiniteSamps, 
                                    SAMPLES_PER_CHANNEL ));

  DAQmxErrChk(DAQmxRegisterEveryNSamplesEvent(
      taskHandle, DAQmx_Val_Acquired_Into_Buffer, SAMPLES_PER_SEC, 0,
      &EveryNCallback, BUFFER_SIZE));


  //Add DAQmxRegisterEveryNSamplesEvent



  //Add functions DoneCallback and EveryNEvents


  DAQmxErrChk(DAQmxRegisterDoneEvent(taskHandle, 0, DoneCallback, NULL));


  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  DAQmxErrChk(DAQmxStartTask(taskHandle));

  

  Error:
  if (DAQmxFailed(error)) {
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    printf("DAQmx Error: %s\n", errBuff);
  }

  //printf("Task started!\n");

  // DAQmx Read Code -- i.e: take samples
  // The samples are written interleaved with the GroupByScanNumber
  /*
  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, SAMPLES_PER_CHANNEL, 
                                 SAMPLES_WAIT_TIMEOUT_SECS, 
                                 DAQmx_Val_GroupByScanNumber,
                                 //DAQmx_Val_GroupByChannel, 
                                 data, ARRAY_SIZE_IN_SAMPLES, 
                                 &samples_read_per_channel, 
                                 NULL));
                                 */
  

  /*

  // DAQmx Stop and clear task
  Error:
  if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  }
  if( taskHandle != 0 )  {
    //printf("NIDAQ testing complete!\n");

    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);

    //printf("We read [%d] samples for each channel\n", samples_read_per_channel);
    //printf("We took [%d] samples per second\n", SAMPLES_PER_SEC);

    // Print out the data we collected on differences across the paired pins

    float64 power;
    float64 time;
    int i,j;

    // Print the header of the csv
    fprintf(fp, "time, ");
    for(i = 0; i < NUM_CHANNEL_PAIRS-1; i++){
      //fprintf(fp, "line%d, ",i);
      fprintf(fp, "line%d (%.1f), ",i, voltage_array[i]);
    }
    
    //fprintf(fp, "line%d\n",i);
    fprintf(fp, "line%d (%.1f)\n",i, voltage_array[i]);

    // Print the data
    for(i = 0; i < ARRAY_SIZE_IN_SAMPLES; i+=NUM_CHANNEL_PAIRS){
      time = (float)i/(NUM_CHANNEL_PAIRS*SAMPLES_PER_SEC);
      fprintf(fp, "%2.6f, ", time);

      for(j = 0; j < NUM_CHANNEL_PAIRS-1; j++){
        //power = (data[i+j]/RESISTOR_OHMS)*LINE_VOLTAGE;
        power = (data[i+j]/RESISTOR_OHMS)*voltage_array[j];
        //fprintf(fp, "%2.6f (%f), ", power, voltage_array[j]);
        fprintf(fp, "%2.6f, ", power);
      }
      //power = (data[i+j]/RESISTOR_OHMS)*LINE_VOLTAGE;
      power = (data[i+j]/RESISTOR_OHMS)*voltage_array[NUM_CHANNEL_PAIRS-1];
      //fprintf(fp, "%2.6f (%f)\n", power, voltage_array[NUM_CHANNEL_PAIRS-1]);
      fprintf(fp, "%2.6f\n", power);
    }
  }
  if( DAQmxFailed(error) ){
    //printf("DAQmx Error: %s\n",errBuff);
    fprintf(fp, "DAQmx Error: %s\n",errBuff);
    return 0;
  }
  */  

  return 0;
}
