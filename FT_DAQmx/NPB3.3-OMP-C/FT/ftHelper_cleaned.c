#include <stdio.h> 
#include "ftHelper.h"
#include <NIDAQmx.h>

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) finalize(); else

  static void createTask() {
    int32 createTaskStatus = DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle);
    if (DAQmxFailed(createTaskStatus)) {
      char errBuff[2048];
      DAQmxGetExtendedErrorInfo(errBuff, 2048);
      printf("DAQmx Initialization Error: %s\n", errBuff);
    }
    printf("createTaskStatus = %d\n", createTaskStatus);
    DAQmxErrChk (createTaskStatus);
    printf("After createTask() ---> taskHandle = %d\n", taskHandle);
  }

  static void createAIVoltageChan() {
    printf("---> createAIVoltageChan (ftHelper.c)\n");
    // Start in differential mode                                                 
    DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle,                              
                                       PHYS_CHANNELS,
                                       CHANNEL_NAME,                            
                                       DAQmx_Val_Diff,                     
                                       MIN_VOLTS, MAX_VOLTS,                    
                                       DAQmx_Val_Volts, NULL));
  }

  static void setSampleClockAndRate() {
    // Setup the sample clock and the rate at which we collect samples
    printf("---> setSampleClockRate() Called\n");
    DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, NULL, SAMPLES_PER_SEC,          
                                    DAQmx_Val_Rising,                           
                                    DAQmx_Val_FiniteSamps,                      
                                    SAMPLES_PER_CHANNEL ));
  }

  static void startTask() {
    // DAQmx Start Code
    printf("---> startTask() Called\n");                                                           
    DAQmxStartTask(taskHandle);
    printf("startTask() ---> taskHandle = %d\n", taskHandle);
  }

  static void finalize() {
    printf("---> finalize() Called\n");
    //-- open input file --
    FILE *fp;
    fp = fopen("DAQmx_Measurements.csv", "w");
    
    // DAQmx Stop and clear task
    if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  }
  if( taskHandle != 0 )  {
    printf("NIDAQ testing complete!\n");

    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);

    // Print out the data we collected on differences across the paired pins
    float64 power;
    float64 time;
    int i,j;

    // Print the header of the csv
    fprintf(fp, "time, ");

    for(i = 0; i < NUM_CHANNEL_PAIRS-1; i++){
      fprintf(fp, "line%d, ",i);
    }
    fprintf(fp, "line%d\n",i);

    // Print the data
    for(i = 0; i < ARRAY_SIZE_IN_SAMPLES; i+=NUM_CHANNEL_PAIRS){
      time = (float)i/(NUM_CHANNEL_PAIRS*SAMPLES_PER_SEC);
      fprintf(fp, "%2.6f, ", time);
      for(j = 0; j < NUM_CHANNEL_PAIRS-1; j++){
        power = (data[i+j]/RESISTOR_OHMS)*LINE_VOLTAGE;
        fprintf(fp, "%2.6f, ", power);
      }
      power = (data[i+j]/RESISTOR_OHMS)*LINE_VOLTAGE;
      fprintf(fp, "%2.6f\n", power);
    }
  }
  if( DAQmxFailed(error) ){
    printf("DAQmx Error: %s\n",errBuff);
    //return 0;
  }
  }


  static void takeSamples() {             
  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, SAMPLES_PER_CHANNEL,               
                                 SAMPLES_WAIT_TIMEOUT_SECS,                     
                                 DAQmx_Val_GroupByScanNumber,               
                                 data, 
                                 ARRAY_SIZE_IN_SAMPLES,                   
                                 &samples_read_per_channel,                     
                                 NULL));                                                               
  }

  void beginTask() {
  printf("beginTask() Called\n");
  createTask();
  createAIVoltageChan();
  setSampleClockAndRate();
  startTask();
  takeSamples();
  }

  void endTask() {
    printf("endTask() Called\n");
    finalize();
  }