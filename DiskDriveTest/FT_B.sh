#!/bin/bash

SAMPLES_PER_SEC=1000
#SAMPLING_TIME_IN_SECS=90
SAMPLING_TIME_IN_SECS=20
START_BENCH_DELAY_SECS=2
VERSION=3
OUTPUT_FILE=FT_B_DAQmxV${VERSION}.csv
OUTPUT_DIRECTORY=April_4_FT_BV3
TIMESTAMP_FILE=TimeStamp_April4V3.csv

echo "Starting MLPerf Combined! Writing to file: ${OUTPUT_FILE}"

#Take NIDAQmx Measurements (in the background)
/home/cesar/Desktop/testNIDAQ_CesarV2/DiskDriveTest/main $SAMPLES_PER_SEC $SAMPLING_TIME_IN_SECS $OUTPUT_FILE > ${OUTPUT_FILE} &
waitpid1=$!

#sudo /opt/AMDuProf_4.1-424/bin/AMDuProfCLI timechart --interval 100 --duration 60 --event frequency --event p-state --event power --event temperature -o /root//.AMDuProf/AMDuProf/ &
sudo /opt/AMDuProf_4.1-424/bin/AMDuProfCLI timechart --interval 10 --duration 20 --event power -o /home/cesar/Desktop/CPU_Only_Benchmark/AMDuProf_Energy_Results/${OUTPUT_DIRECTORY} &
waitpid2=$!


/home/cesar/Desktop/testNIDAQ_CesarV2/NPB/SNU_NPB_2019/NPB3.3-OMP-C/bin/ft.B.x

# Start MLPerf Inference Benchmark

#cm run script "app mlperf inference generic _python _retinanet _onnxruntime _cpu" \
#     --scenario=Offline \
#     --mode=accuracy \
#     --test_query_count=10 \
#     --rerun > ${CM_OUTPUT_FILE}

#cm run script "app mlperf inference generic _python _retinanet _onnxruntime _cpu" \
#     --scenario=Offline \
#     --mode=performance \
#     --rerun \
#     --num_threads=4

echo "FT Class B Complete!"
echo "Waiting for NIDAQ sampling to finish..."

# Wait for sampling to finish
wait $waitpid1
echo "Waiting for AMDuProf sampling to finish..."
wait $waitpid2

echo "FT Class B complete!"
