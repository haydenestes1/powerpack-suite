
#include <stdlib.h>
#include <stdio.h>
#include <NIDAQmx.h>

//Includes for writing to temp files
#include<unistd.h>
#include<string.h>
#include<errno.h>

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// This is the name of the DAQ device
#define DAQ_DEVICE_NAME "cDAQ1"

// This is the name of the module port we measure from
#define DAQ_MODULE_NAME "Mod3"
#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME

// The END channel is INCLUSIVE
// These are the pin indices used for DIFFERENTIAL mode
// Pin X will automatically be paired with its corresponding
// x+8 pin. 
// This setup assumes we are using pins 0, 1, 2, and 3
// with their complementary pins 8, 9, 10, and 11
// which are automatically read by the NIDAQ library
// when we are in differential mode
#define DIFF_INPUTS_BEGIN "0"
#define DIFF_INPUTS_END   "6"

#define DIFF_INPUTS_12V_BEGIN "0"
#define DIFF_INPUTS_12V_END   "4"

#define DIFF_INPUTS_5V_BEGIN "5"
#define DIFF_INPUTS_5V_END   "6"

#define DIFF_INPUTS_3V_BEGIN "6"
#define DIFF_INPUTS_3V_END   "7"





// We'll pass this into the CreateAIVoltageChan to specify
// the channels we want to work with. 
#define DIFF_CHANNELS_12V DAQ_DEVICE "/ai" DIFF_INPUTS_12V_BEGIN ":" DIFF_INPUTS_12V_END 
#define PHYS_CHANNELS_12V DIFF_CHANNELS_12V
#define DIFF_CHANNELS_5V DAQ_DEVICE "/ai" DIFF_INPUTS_5V_BEGIN ":" DIFF_INPUTS_5V_END 
#define PHYS_CHANNELS_5V DIFF_CHANNELS_5V
#define DIFF_CHANNELS_3V DAQ_DEVICE "/ai" DIFF_INPUTS_3V_BEGIN ":" DIFF_INPUTS_3V_END 
#define PHYS_CHANNELS_3V DIFF_CHANNELS_3V

// The name assigned to our task
#define DAQ_TASK_NAME ""

// The name we assign to our voltage channel
#define CHANNEL_NAME ""

// The minimum and maximum volts we expect to measure
// The NIDAQ device only supports max of -10/10 volts
// This is not good if we have a 12V supply we want to measure
#define MIN_VOLTS -10.0
#define MAX_VOLTS  10.0

// Number of samples to collect each second for each channel
//#define SAMPLES_PER_SEC 1000

// The number of samples we want to take for each channel
//#define SAMPLES_PER_CHANNEL 16000
//#define SAMPLES_PER_CHANNEL 30000

// The amount of time to wait to read the samples
#define SAMPLES_WAIT_TIMEOUT_SECS 100

// The number of differential channel pairs we will read from
//#define NUM_CHANNEL_PAIRS 4
#define NUM_CHANNEL_PAIRS_12V 5
#define NUM_CHANNEL_PAIRS_5V 1
#define NUM_CHANNEL_PAIRS_3V 1
#define NUM_CHANNEL_PAIRS 7

// The number of samples we expect to collect
#define ARRAY_SIZE_IN_SAMPLES NUM_CHANNEL_PAIRS*SAMPLES_PER_CHANNEL
#define ARRAY_SIZE_IN_SAMPLES_12V NUM_CHANNEL_PAIRS_12V*SAMPLES_PER_CHANNEL
#define ARRAY_SIZE_IN_SAMPLES_5V NUM_CHANNEL_PAIRS_5V*SAMPLES_PER_CHANNEL
#define ARRAY_SIZE_IN_SAMPLES_3V NUM_CHANNEL_PAIRS_3V*SAMPLES_PER_CHANNEL

// The resistance of the resister that we measure the voltage diff over
#define RESISTOR_OHMS 0.003

// The voltage we assume that all the lines are running at
#define LINE_VOLTAGE 12
#define LINE_VOLTAGE_12V 12
#define LINE_VOLTAGE_5V 12
#define LINE_VOLTAGE_3V 12

#define ARRAY_SIZE 7
#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}


int main(int argc, char** argv){

  float voltage_array[ARRAY_SIZE] = INIT_ARRAY;

  //printf("Starting tests of NIDAQ unit...\n");
  //printf("Using device: [%s]\n", DAQ_DEVICE);
  //printf("Using channels: [%s]\n", PHYS_CHANNELS);
  
  if(argc != 4){
    printf("Invalid input argument count. Given %d, Expected %d\n", argc-1, 2);
    printf("Usage: ./main [SAMPLES_PER_SECOND] [SAMPLING_TIME_IN_SECONDS]\n");
    exit(-1);
  }

  int SAMPLES_PER_SEC = atoi(argv[1]);
  int SAMPLING_SECS = atoi(argv[2]);
  int SAMPLES_PER_CHANNEL = SAMPLING_SECS*SAMPLES_PER_SEC;

  //-- open input file --
  FILE *fp;
  fp = fopen(argv[3], "w");

  FILE *fp12V;
  fp12V = fopen("12V_Measurements.csv", "w");
  FILE *fp5V;
  fp5V = fopen("5V_Measurements.csv", "w");
  FILE *fp3V;
  fp3V = fopen("3V_Measurements.csv", "w");

  int32       error = 0;
  TaskHandle  taskHandle12V = 0;
  TaskHandle  taskHandle5V = 0;
  TaskHandle  taskHandle3V = 0;
  int32       samples_read_per_channel;
  float64     data[ARRAY_SIZE_IN_SAMPLES];
  float64     data12V[ARRAY_SIZE_IN_SAMPLES_12V];
  float64     data5V[ARRAY_SIZE_IN_SAMPLES_5V];
  float64     data3V[ARRAY_SIZE_IN_SAMPLES_3V];
  char        errBuff[2048]={'\0'};

  //printf("Starting task creation!\n");

  DAQmxErrChk (DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle12V));
  DAQmxErrChk (DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle5V));
  DAQmxErrChk (DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle3V));

  //printf("Task created!\n");

  // Start 12V in differential mode
  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle12V, 
                                       PHYS_CHANNELS_12V, 
                                       //"cDAQ1Mod3/ai0:3, cDAQ1Mod3/ai8:11",
                                       CHANNEL_NAME, 
                                       DAQmx_Val_Diff, 
                                       //DAQmx_Val_NRSE,
                                       //DAQmx_Val_RSE,
                                       MIN_VOLTS, MAX_VOLTS, 
                                       DAQmx_Val_Volts, NULL));

  // Start 5V in differential mode
  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle5V, 
                                       PHYS_CHANNELS_5V, 
                                       //"cDAQ1Mod3/ai0:3, cDAQ1Mod3/ai8:11",
                                       CHANNEL_NAME, 
                                       DAQmx_Val_Diff, 
                                       //DAQmx_Val_NRSE,
                                       //DAQmx_Val_RSE,
                                       MIN_VOLTS, MAX_VOLTS, 
                                       DAQmx_Val_Volts, NULL));
  
  // Start 3V in differential mode
  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle3V, 
                                       PHYS_CHANNELS_3V, 
                                       //"cDAQ1Mod3/ai0:3, cDAQ1Mod3/ai8:11",
                                       CHANNEL_NAME, 
                                       DAQmx_Val_Diff, 
                                       //DAQmx_Val_NRSE,
                                       //DAQmx_Val_RSE,
                                       MIN_VOLTS, MAX_VOLTS, 
                                       DAQmx_Val_Volts, NULL));

  //printf("Voltage Chan created!\n");

  // Setup the sample clock and the rate at which we collect samples
  DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle12V, NULL, SAMPLES_PER_SEC, 
                                    DAQmx_Val_Rising, 
                                    DAQmx_Val_FiniteSamps, 
                                    SAMPLES_PER_CHANNEL ));

  // Setup the sample clock and the rate at which we collect samples
  DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle5V, NULL, SAMPLES_PER_SEC, 
                                    DAQmx_Val_Rising, 
                                    DAQmx_Val_FiniteSamps, 
                                    SAMPLES_PER_CHANNEL ));

  // Setup the sample clock and the rate at which we collect samples
  DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle3V, NULL, SAMPLES_PER_SEC, 
                                    DAQmx_Val_Rising, 
                                    DAQmx_Val_FiniteSamps, 
                                    SAMPLES_PER_CHANNEL ));

  //printf("Sampling rate set!\n");

  // DAQmx Start Code
  printf("Before DAQmxStartTask(taskHandle12V)\n");
  DAQmxErrChk(DAQmxStartTask(taskHandle12V));
  printf("After DAQmxStartTask(taskHandle12V)\n");
  printf("Before DAQmxStartTask(taskHandle5V)\n");
  // DAQmx Start Code
  DAQmxErrChk(DAQmxStartTask(taskHandle5V));
  printf("After DAQmxStartTask(taskHandle5V)\n");
  printf("Before DAQmxStartTask(taskHandle3V)\n");
  // DAQmx Start Code
  DAQmxErrChk(DAQmxStartTask(taskHandle3V));
  printf("After DAQmxStartTask(taskHandle3V)\n");

  //printf("Task started!\n");

  // DAQmx Read Code -- i.e: take samples
  // The samples are written interleaved with the GroupByScanNumber
  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle12V, SAMPLES_PER_CHANNEL, 
                                 SAMPLES_WAIT_TIMEOUT_SECS, 
                                 DAQmx_Val_GroupByScanNumber,
                                 //DAQmx_Val_GroupByChannel, 
                                 data12V, ARRAY_SIZE_IN_SAMPLES_12V, 
                                 &samples_read_per_channel, 
                                 NULL));

  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle5V, SAMPLES_PER_CHANNEL, 
                                 SAMPLES_WAIT_TIMEOUT_SECS, 
                                 DAQmx_Val_GroupByScanNumber,
                                 //DAQmx_Val_GroupByChannel, 
                                 data5V, ARRAY_SIZE_IN_SAMPLES_5V, 
                                 &samples_read_per_channel, 
                                 NULL));
  
  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle3V, SAMPLES_PER_CHANNEL, 
                                 SAMPLES_WAIT_TIMEOUT_SECS, 
                                 DAQmx_Val_GroupByScanNumber,
                                 //DAQmx_Val_GroupByChannel, 
                                 data3V, ARRAY_SIZE_IN_SAMPLES_3V, 
                                 &samples_read_per_channel, 
                                 NULL));


  // DAQmx Stop and clear task
Error:
  if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  }
  if( taskHandle12V != 0 ||  taskHandle5V != 0 ||  taskHandle3V != 0 )  {
    //printf("NIDAQ testing complete!\n");

    DAQmxStopTask(taskHandle12V);
    DAQmxClearTask(taskHandle12V);

    DAQmxStopTask(taskHandle5V);
    DAQmxClearTask(taskHandle5V);

    DAQmxStopTask(taskHandle3V);
    DAQmxClearTask(taskHandle3V);

    // Print out the data we collected on differences across the paired pins

    //-- Beginning of 12V Print

    float64 power12V;
    float64 time12V;
    int i12V,j12V;

    // Print the header of the csv
    fprintf(fp12V, "time, ");
    for(i12V = 0; i12V < NUM_CHANNEL_PAIRS_12V-1; i12V++){
      fprintf(fp12V, "line%d, ",i12V);
    }
    fprintf(fp12V, "line%d\n",i12V);

    // Print the data
    for(i12V = 0; i12V < ARRAY_SIZE_IN_SAMPLES_12V; i12V+=NUM_CHANNEL_PAIRS_12V){
      time12V = (float)i12V/(NUM_CHANNEL_PAIRS_12V*SAMPLES_PER_SEC);
      
      fprintf(fp12V, "%2.6f, ", time12V);
      for(j12V = 0; j12V < NUM_CHANNEL_PAIRS_12V-1; j12V++){
        power12V = (data[i12V+j12V]/RESISTOR_OHMS)*LINE_VOLTAGE_12V;
        fprintf(fp12V, "%2.6f, ", power12V);
      }
      power12V = (data[i12V+j12V]/RESISTOR_OHMS)*LINE_VOLTAGE_12V;
      fprintf(fp12V, "%2.6f\n", power12V);
    }
  

  //-- End of 12V Print --
  //-- Beginning of 5V Print --

    float64 power5V;
    float64 time5V;
    int i5V,j5V;

    // Print the header of the csv
    fprintf(fp5V, "time, ");
    for(i5V = 0; i5V < NUM_CHANNEL_PAIRS_5V-1; i5V++){
      fprintf(fp5V, "line%d, ",i5V);
    }
    fprintf(fp5V, "line%d\n",i5V);

    // Print the data
    for(i5V = 0; i5V < ARRAY_SIZE_IN_SAMPLES_5V; i5V+=NUM_CHANNEL_PAIRS_5V){
      time5V = (float)i5V/(NUM_CHANNEL_PAIRS_5V*SAMPLES_PER_SEC);
      
      fprintf(fp5V, "%2.6f, ", time5V);
      for(j5V = 0; j5V < NUM_CHANNEL_PAIRS_5V-1; j5V++){
        power5V = (data[i5V+j5V]/RESISTOR_OHMS)*LINE_VOLTAGE_5V;
        fprintf(fp5V, "%2.6f, ", power5V);
      }
      power5V = (data[i5V+j5V]/RESISTOR_OHMS)*LINE_VOLTAGE_5V;
      fprintf(fp5V, "%2.6f\n", power5V);
    }

    //-- End of 5V Print
    //-- Beginning of 3V Print --

    float64 power3V;
    float64 time3V;
    int i3V,j3V;

    // Print the header of the csv
    fprintf(fp3V, "time, ");
    for(i3V = 0; i3V < NUM_CHANNEL_PAIRS_3V-1; i3V++){
      fprintf(fp3V, "line%d, ",i3V);
    }
    fprintf(fp3V, "line%d\n",i3V);

    // Print the data
    for(i3V = 0; i3V < ARRAY_SIZE_IN_SAMPLES_3V; i3V+=NUM_CHANNEL_PAIRS_3V){
      time3V = (float)i3V/(NUM_CHANNEL_PAIRS_3V*SAMPLES_PER_SEC);
      
      fprintf(fp3V, "%2.6f, ", time3V);
      for(j3V = 0; j3V < NUM_CHANNEL_PAIRS_3V-1; j3V++){
        power3V = (data[i3V+j3V]/RESISTOR_OHMS)*LINE_VOLTAGE_3V;
        fprintf(fp3V, "%2.6f, ", power3V);
      }
      power3V = (data[i3V+j3V]/RESISTOR_OHMS)*LINE_VOLTAGE_3V;
      fprintf(fp3V, "%2.6f\n", power3V);
    }

    //-- End of 3V Print



  }
  if( DAQmxFailed(error) ){
    fprintf(fp12V, "DAQmx Error: %s\n",errBuff);
    return 0;
  }

  


  return 0;
}


