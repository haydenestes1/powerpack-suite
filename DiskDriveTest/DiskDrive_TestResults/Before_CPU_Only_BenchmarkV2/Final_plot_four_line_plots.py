import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import pandas as pd
from scipy.interpolate import *
import sys

plt.rcParams['figure.figsize'] = [12, 5]
plt.rcParams['figure.dpi'] = 100

#os.chdir("/content/drive/MyDrive/Advanced_Systems_Final_Project/")

if len(sys.argv) < 2:
    print("Using Default parameter string.")
    parameter_string = 'MLPerf_Inference_Performance.csv'
else:
    parameter_string = sys.argv[1]
    print(f"Parameter string received: {parameter_string}")

print("parameter_string = ", parameter_string)
df = pd.read_csv(parameter_string)

lines = df.columns[1:]
print(lines)

# Define a list of distinct colors
line_colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']

# Average out the lines
df['mean'] = df[lines].mean(axis=1)

print(df.tail(10))
print(df.shape)
print(df.columns)

# Create a directory to save the individual line plots
if not os.path.exists("line_plots"):
    os.makedirs("line_plots")

# Create a figure for the four plots
fig, axes = plt.subplots(2, 2, figsize=(15, 10))

# Plot and save individual line plots with distinct colors
for i, line in enumerate(lines):
    row, col = i // 2, i % 2  # Determine the subplot position
    ax = axes[row, col]

    ax.plot(df.time, df[line], '--', linewidth=2, label=line, color=line_colors[i])
    ax.set_title(f'CPU Power Line Consumption Over Time for {line}')
    ax.set_ylabel('Power (watts)')
    ax.set_xlabel('Time (seconds)')
    ax.legend()

# Adjust layout and save the figure as 'mean_lines.jpg'
plt.tight_layout()
plt.savefig('all_lines.jpg')

# Plot and save the mean line plot
x = df['time']
y = df['mean']

fig, ax = plt.subplots(figsize=(15,10))

ax.plot(df.time, df['mean'], '--', linewidth=2, c='black')
ax.scatter(x, y, c='red', label='Raw Data')

# Make interpolation spline as summary data
cs = CubicSpline(x, y)
xnew = np.linspace(x.min(), x.max(), 15)
ynew = cs(xnew)

ax.plot(xnew, ynew, label='Interpolated Data', linewidth=3)

ax.legend()

ax.set_title('CPU Lines -- Mean Power Consumption Over Time')
ax.set_ylabel('Mean Power (watts) From All Lines')
ax.set_xlabel('Time (seconds)')

plt.savefig('meanLines.jpg')
