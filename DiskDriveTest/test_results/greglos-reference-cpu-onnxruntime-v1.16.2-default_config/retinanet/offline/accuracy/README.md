This experiment is generated using [MLCommons CM](https://github.com/mlcommons/ck)
## CM Run Command
```
cm run script \
	app mlperf inference generic _python _retinanet _onnxruntime _cpu \
	--scenario=Offline \
	--mode=accuracy \
	--test_query_count=10 \
	--rerun
```