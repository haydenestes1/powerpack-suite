
#include <stdlib.h>
#include <stdio.h>
#include <NIDAQmx.h>

//Includes for writing to temp files
#include<unistd.h>
#include<string.h>
#include<errno.h>

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

// This is the name of the DAQ device
//#define DAQ_DEVICE_NAME "cDAQ1"
#define DAQ_DEVICE_NAME "cDAQ2"

// This is the name of the module port we measure from
#define DAQ_MODULE_NAME "Mod3"  //12V
#define DAQ_MODULE_NAME2 "Mod6"  //5V
#define DAQ_MODULE_NAME3 "Mod8"  //3V

//#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME
#define DAQ_DEVICE DAQ_DEVICE_NAME DAQ_MODULE_NAME

// The END channel is INCLUSIVE
// These are the pin indices used for DIFFERENTIAL mode
// Pin X will automatically be paired with its corresponding
// x+8 pin. 
// This setup assumes we are using pins 0, 1, 2, and 3
// with their complementary pins 8, 9, 10, and 11
// which are automatically read by the NIDAQ library
// when we are in differential mode
#define DIFF_INPUTS_BEGIN_V1 "0"
//#define DIFF_INPUTS_END   "3"
#define DIFF_INPUTS_END_V1   "4"

#define DIFF_INPUTS_BEGIN_V2 "0"
#define DIFF_INPUTS_END_V2   "0"

#define DIFF_INPUTS_BEGIN_V3 "0"
#define DIFF_INPUTS_END_V3   "0"

// We'll pass this into the CreateAIVoltageChan to specify
// the channels we want to work with. 
//#define DIFF_CHANNELS DAQ_DEVICE "/ai" DIFF_INPUTS_BEGIN_V1 ":" DIFF_INPUTS_END_V1 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME2 "/ai" DIFF_INPUTS_BEGIN_V2 ":" DIFF_INPUTS_END_V2
#define DIFF_CHANNELS DAQ_DEVICE "/ai" DIFF_INPUTS_BEGIN_V1 ":" DIFF_INPUTS_END_V1 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME2 "/ai" DIFF_INPUTS_BEGIN_V2 ":" DIFF_INPUTS_END_V2 "," DAQ_DEVICE_NAME DAQ_MODULE_NAME3 "/ai" DIFF_INPUTS_BEGIN_V3 ":" DIFF_INPUTS_END_V3
#define PHYS_CHANNELS DIFF_CHANNELS

// The name assigned to our task
#define DAQ_TASK_NAME ""

// The name we assign to our voltage channel
#define CHANNEL_NAME ""

// The minimum and maximum volts we expect to measure
// The NIDAQ device only supports max of -10/10 volts
// This is not good if we have a 12V supply we want to measure
#define MIN_VOLTS -10.0
#define MAX_VOLTS  10.0

// Number of samples to collect each second for each channel
//#define SAMPLES_PER_SEC 1000

// The number of samples we want to take for each channel
//#define SAMPLES_PER_CHANNEL 16000
//#define SAMPLES_PER_CHANNEL 30000

// The amount of time to wait to read the samples
#define SAMPLES_WAIT_TIMEOUT_SECS 100

// The number of differential channel pairs we will read from
//#define NUM_CHANNEL_PAIRS 4
#define NUM_CHANNEL_PAIRS 7

// The number of samples we expect to collect
#define ARRAY_SIZE_IN_SAMPLES NUM_CHANNEL_PAIRS*SAMPLES_PER_CHANNEL

// The resistance of the resister that we measure the voltage diff over
#define RESISTOR_OHMS 0.003

// The voltage we assume that all the lines are running at
#define LINE_VOLTAGE 12

//#define ARRAY_SIZE 7
//#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}
#define ARRAY_SIZE 7
//#define INIT_ARRAY {12, 12, 12, 12, 12, 5, 3.3}
#define INIT_ARRAY {12, 12, 12, 12, 12, 3.3, 5}


int main(int argc, char** argv){

  float voltage_array[ARRAY_SIZE] = INIT_ARRAY;

  //printf("Starting tests of NIDAQ unit...\n");
  //printf("Using device: [%s]\n", DAQ_DEVICE);
  //printf("Using channels: [%s]\n", PHYS_CHANNELS);
  
  if(argc != 4){
    printf("Invalid input argument count. Given %d, Expected %d\n", argc-1, 2);
    printf("Usage: ./main [SAMPLES_PER_SECOND] [SAMPLING_TIME_IN_SECONDS]\n");
    exit(-1);
  }

  int SAMPLES_PER_SEC = atoi(argv[1]);
  int SAMPLING_SECS = atoi(argv[2]);
  int SAMPLES_PER_CHANNEL = SAMPLING_SECS*SAMPLES_PER_SEC;

  //-- open input file --
  FILE *fp;
  fp = fopen(argv[3], "w");
  FILE *data_fp;
  data_fp = fopen("CPU_7Lines_Data_OutputV3.csv", "w");

  int32       error = 0;
  TaskHandle  taskHandle = 0;
  int32       samples_read_per_channel;
  float64     data[ARRAY_SIZE_IN_SAMPLES];
  char        errBuff[2048]={'\0'};

  //---- Printing output to CSV in temp -----
  //FILE *fpt;
  //fpt = fopen("Main_Output.csv", "w+");

  //printf("Starting task creation!\n");

  DAQmxErrChk (DAQmxCreateTask(DAQ_TASK_NAME, &taskHandle));

  // Start in differential mode
  DAQmxErrChk(DAQmxCreateAIVoltageChan(taskHandle, 
                                       PHYS_CHANNELS, 
                                       CHANNEL_NAME, 
                                       DAQmx_Val_Cfg_Default,
                                       MIN_VOLTS, MAX_VOLTS, 
                                       DAQmx_Val_Volts, NULL));

  //printf("Voltage Chan created!\n");

  // Setup the sample clock and the rate at which we collect samples
  DAQmxErrChk(DAQmxCfgSampClkTiming(taskHandle, NULL, SAMPLES_PER_SEC, 
                                    DAQmx_Val_Rising, 
                                    //DAQmx_Val_FiniteSamps, 
                                    DAQmx_Val_ContSamps,
                                    SAMPLES_PER_CHANNEL ));

  //printf("Sampling rate set!\n");

  // DAQmx Start Code
  DAQmxErrChk(DAQmxStartTask(taskHandle));

  //printf("Task started!\n");

  // DAQmx Read Code -- i.e: take samples
  // The samples are written interleaved with the GroupByScanNumber
  DAQmxErrChk(DAQmxReadAnalogF64(taskHandle, SAMPLES_PER_CHANNEL, 
                                 SAMPLES_WAIT_TIMEOUT_SECS, 
                                 //DAQmx_Val_GroupByScanNumber,
                                 DAQmx_Val_GroupByChannel, 
                                 data, ARRAY_SIZE_IN_SAMPLES, 
                                 &samples_read_per_channel, 
                                 NULL));

  // DAQmx Stop and clear task
Error:
  if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
  }
  if( taskHandle != 0 )  {

    DAQmxStopTask(taskHandle);
    DAQmxClearTask(taskHandle);

    // Print out the data we collected on differences across the paired pins
    float64 power;
    float64 time;
    float64 daq_value;
    float64 adjusted_voltage;
    int i,j;

    // Print the header of the csv
    fprintf(fp, "time, ");
    fprintf(data_fp, "time, ");
    for(i = 0; i < NUM_CHANNEL_PAIRS-1; i++){
      //fprintf(fp, "line%d, ",i);
      fprintf(fp, "line%d (%.1fV), ",i, voltage_array[i]);
      fprintf(data_fp, "line%d (%.1fV), ",i, voltage_array[i]);
    }
    
    //fprintf(fp, "line%d\n",i);
    fprintf(fp, "line%d (%.1fV)\n",i, voltage_array[i]);
    fprintf(data_fp, "line%d (%.1fV)\n",i, voltage_array[i]);

    // Print the data (greg's version)
    for(i = 0; i < SAMPLES_PER_CHANNEL; i++){
      time = (float)i/(SAMPLES_PER_SEC);
      fprintf(fp, "%2.6f, ", time);
      fprintf(data_fp, "%2.6f, ", time);

      for(j = 0; j < NUM_CHANNEL_PAIRS-1; j++){
        
        daq_value = data[i + j*SAMPLES_PER_CHANNEL];
        //adjusted_voltage = voltage_array[j] - RESISTOR_OHMS;
        adjusted_voltage = voltage_array[j];
        power = (daq_value / RESISTOR_OHMS) * (adjusted_voltage - daq_value);
     
        fprintf(fp, "%2.6f, ", power);
        fprintf(data_fp, "%2.6f, ", daq_value);
      }

      daq_value = data[i + j*SAMPLES_PER_CHANNEL];
      //adjusted_voltage = voltage_array[j] - RESISTOR_OHMS;
      adjusted_voltage = voltage_array[j];
      power = (daq_value / RESISTOR_OHMS) * (adjusted_voltage - daq_value);
      
      fprintf(fp, "%2.6f\n", power);
      fprintf(data_fp, "%2.6f\n", daq_value);
    }

  }
  if( DAQmxFailed(error) ){
    //printf("DAQmx Error: %s\n",errBuff);
    fprintf(fp, "DAQmx Error: %s\n",errBuff);
    return 0;
  }

  return 0;
}