import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sys

if len(sys.argv) < 2:
    print("Using Default parameter string.")
    parameter_string = '/home/cesar/Desktop/testNIDAQ_CesarV2/DiskDriveTest/FT_B_DAQmxV3.csv'
else:
    parameter_string = sys.argv[1]
    print(f"Parameter string received: {parameter_string}")

# Read the first two rows to get column names and skip the first two rows
df = pd.read_csv(parameter_string, skiprows=1)

# Remove whitespace from column names
df.columns = [col.strip() for col in df.columns]

# Calculate sum for each category
cpu_power_sum = df[df.columns[1:5]].sum(axis=1)
gpu_power_sum = pd.concat([df[df.columns[5:9]], df[df.columns[10:12]]], axis=1).sum(axis=1)
disk_power_sum = pd.concat([df[df.columns[9:10]], df[df.columns[12:14]]], axis=1).sum(axis=1)

# Create a figure for the three plots
fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(15, 20))

# Plotting CPU Power Consumption with red color 
axes[0].plot(df['time'], cpu_power_sum, color='red')
axes[0].set_title('CPU Power Consumption')
axes[0].set_ylabel('Power (watts)')
axes[0].set_xlabel('Time (seconds)')

# Plotting GPU Power Consumption with green color 
axes[1].plot(df['time'], gpu_power_sum, color='green')
axes[1].set_title('GPU Power Consumption')
axes[1].set_ylabel('Power (watts)')
axes[1].set_xlabel('Time (seconds)')

# Plotting Disk Power Consumption with blue color 
axes[2].plot(df['time'], disk_power_sum, color='blue')
axes[2].set_title('Disk Power Consumption')
axes[2].set_ylabel('Power (watts)')
axes[2].set_xlabel('Time (seconds)')

# Adjust layout and save the figure
plt.tight_layout()
plt.savefig('Power_Consumption_Graphs.jpg')
