import pandas as pd

# Load the CSV file
df = pd.read_csv('power_measurements.csv')

# Calculate the time interval in seconds
delta_t = 0.001  # 1 millisecond = 0.001 seconds

# Calculate the energy for each column (except the time column)
energy_consumption = {}
for column in df.columns[1:]:
    energy_consumption[column] = df[column].sum() * delta_t

# Print the energy consumption for each column
for line, energy in energy_consumption.items():
    print(f'Energy consumption for {line}: {energy} Joules')
